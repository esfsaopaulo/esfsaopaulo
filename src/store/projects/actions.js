export default {
  selectProject({ commit }, payload) {
    commit('selectProject', payload);
  },
};
