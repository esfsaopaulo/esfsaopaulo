export default {
  projectList(state) {
    return state.projectList;
  },

  selectedProject(state) {
    return state.projectList[state.selectedProject];
  },
};
