import Cisterns from './items/cisterns';

export default {
  projectList: [
    Cisterns,
  ],
  selectedProject: -1,
};
