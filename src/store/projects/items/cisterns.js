export default {
  name: `Cisterna na EMEF Desembargador Amorim Lima`,
  image: `cistern.png`,
  program: `Cisternas nas Escolas`,
  description: `Durante 4 meses, voluntários do ESF-SP participaram das aulas regulares
  do sexto ano da EMEF Desembargador Amorim Lima. Ao longo desse período, foram desenvolvidas
  atividades práticas e educativas que relacionavam os conceitos da grade curricular dos alunos
  com o projeto de engenharia da cisterna a ser instalada. Ao final do ciclo, os alunos e voluntários
  construíram em conjunto um sistema de captação de água de chuva de 2.500 litros no pátio da escola.
  A água captada é utilizada atualmente para a lavagem do piso externo da instituição.`,
  theme: `Educação`,
  complexit: `Exige pessoal especializado no planejamento detalhamento e execução`,
  ODS: [
    `Cidades e Comunidades Sustentáveis`,
    `Educação de qualidade`,
    `Água e Saneamento`,
  ],
  objGeneral: `Fomentar o interesse de alunos da rede pública pelas ciências naturais e exatas`,
  objSpecefic: [
    `Aumentar a autonomia dos alunos da EMEF Desembargador Amorim Lima`,
    `Sensibilizar os alunos da EMEF Desembargador Amorim Lima acerca do uso`,
  ],
  status: `Finalizado`,
  begin: `Março 2018`,
  end: `Julho 2018`,
  subject: `Alunos do 6º ano da EMEF Desembargador Amorim Lima`,
  budget: `R$ 1,00`,
  resources: `Edital Amigos da Poli 2017`,
  volunteers: `13`,
  benefited: `100`,
  mapLink: `https://www.google.com.br/maps/place/Escola+Municipal+de+Ensino+Fundamental+Desembargador+Amorim+Lima/@-23.5745612,-46.7314238,17z/data=!3m1!4b1!4m5!3m4!1s0x94ce56676e1212d9:0xfb07953dea9f8564!8m2!3d-23.5745661!4d-46.7292351`,
  videoLink: `https://www.youtube.com/watch?v=sHzyvO5wL9Y`,
  coordinatorsList: [0, 1],
};
