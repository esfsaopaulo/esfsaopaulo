import Julia from './items/julia';
import Joao from './items/joao';

export default {
  memberList: [
    Julia,
    Joao,
  ],
};
