/*********************************************************************
 *                                                                   *
 * name -> nome, tem que estar entre aspas simples                   *
 * program -> prorama, tem que estar entre aspas simples            *
 * image -> nome do arquivo que está na pasta assets/images/members/ *
 *                                                                   *
 *********************************************************************/

export default {
  name: 'João Figueiredo',
  program: 'Cinema',
  image: 'joao.jpg',
};
