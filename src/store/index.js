import Vue from 'vue';
import Vuex from 'vuex';

import projects from './projects';
import members from './members';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    projects: {
      namespaced: true,
      ...projects,
    },
    members: {
      namespaced: true,
      ...members,
    },
  },
});
