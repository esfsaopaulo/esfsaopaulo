import Vue from 'vue';
import Router from 'vue-router';
import { createRouterLayout } from 'vue-router-layout';

const RouterLayout = createRouterLayout(layout => import(`@/layouts/${layout}.vue`));

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/',
      component: RouterLayout,
      children: [
        {
          path: '',
          component: () => import('@/pages/Home.vue'),
        },
        {
          path: 'time',
          component: () => import('@/pages/Team.vue'),
        },
        {
          path: 'projetos',
          component: () => import('@/pages/Projects.vue'),
        },
        {
          path: 'projetos/:id',
          component: () => import('@/pages/ProjectView.vue'),
        },
        {
          path: 'doacao',
          component: () => import('@/pages/Donation.vue'),
        },
        {
          path: 'voluntario',
          component: () => import('@/pages/Volunteer.vue'),
        },
        {
          path: 'requisicao',
          component: () => import('@/pages/Request.vue'),
        }
      ],
    },
  ],
});

export default router;
